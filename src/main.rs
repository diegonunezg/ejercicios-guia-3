use std::io;

fn main() {

    // Si quieren ver un ejercicio lo pueden llamar con una de las funciones
    ej2();
    ej4();
    ej7();

}



// -------------------------------- EJERCICIO 2 ------------------------------------
fn ej2(){
    let numero : i32 = loop { // Se puede otorgar valor a una variable dentro de un loop
        let mut num = String::new();
        let stdin = io::stdin();
        stdin.read_line(&mut num).expect("error al ingresar numero");

        // match evalua 2 posibles casos. parse() intenta convertir un String a un i32
        // y eso puede ser exitoso (Ok) o puede que haya un error (Err)
        let num:i32 = match num.trim().parse() { 
            Ok(bueno) => { 
                            if bueno <= 0 {
                                println!("ingrese un numero positivo");
                                continue
                            } else {
                                bueno
                            }
                         },
            Err(_) => {
                            println!("ingrese numeros no letras");
                            continue
                      },
        };
        break num; // Se puede romper un loop y devolver un valor. Aquí estamos asignando num a numero
    };

    cuenta_atras(numero);
}

fn cuenta_atras(numero:i32) -> i32 {
    println!("{}", numero);
    if numero == 1 { // Caso base. Una cuenta atras termina en 1
        return 1;
    } else { // Si no es 1, la función se llama a si misma restándole 1 al número
        return cuenta_atras(numero-1);
    }
}
// --------------------------- FIN DE EJERCICIO 2 ----------------------------------




// ------------------------------ EJERCICIO 4 --------------------------------------
fn ej4() {
    for i in 1..10{
        println!("{}", tribonacci(i)); 
    }
}

fn tribonacci(numero:i32) -> i32 {
    match numero {
        1 => return 0,
        2 | 3=> return 1,
        _ => return fibonacci(numero-1) + fibonacci(numero-2) + fibonacci(numero-3),
    };
    /*  
        1er termino = 0
        2do termino = 1
        3er termino = 1
        los demas son la suma de los 3 anteriores
    */

}

// --------------------------- FIN EJERCICIO 4 -------------------------------------




// ----------------------------- EJERCICIO 7 ---------------------------------------
fn ej7() {

    let mes = String::from("lunes");
    revisar_semana(&mes); // imprime: lunes es un dia laboral

    let mes = String::from("sabado");
    revisar_semana(&mes); // imprime: sabado no es un dia laboral

    let mes = String::from("popin");
    revisar_semana(&mes); // imprime: popin no es un dia
}

fn revisar_semana(dia:&str) {
    match dia { // Como lo que voy a comparar con match es un String, las opciones tambien deben ser String
        "lunes" | "mates" | "miercoles" | "jueves" | "viernes"  => println!("{} es un dia laboral", dia),
        "sabado" | "domingo" => println!("{} no es un dia laboral", dia),
        _ => println!("{} no es un dia", dia),
    };
}
// -------------------------------- FIN EJERCICIO 7 --------------------------------
